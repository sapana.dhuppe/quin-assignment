package com.assignment.bin.client.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Utils {
    private Properties properties;

    public enum PROPERTY {
        API_KEY("API.KEY"), URL("BIN.API.BASEURL"), ROUTE("BIN.API.ROUTE");
        private String key;
        PROPERTY(String k) {
            this.key = k;
        }

        public String key() {
            return key;
        }
    }

    public Utils() {
        properties = new Properties();
        loadProperties();
    }

    public String get(PROPERTY propertyKey) {
        return properties.getProperty(propertyKey.key());
    }

    // This method is used to load the properties file
    private void loadProperties() {
        InputStream iStream = null;
        try {
            // Loading properties file from the classpath
            iStream = this.getClass().getClassLoader()
                    .getResourceAsStream("test.properties");
            if(iStream == null){
                throw new IOException("File not found");
            }
            properties.load(iStream);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(iStream != null){
                    iStream.close();
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
