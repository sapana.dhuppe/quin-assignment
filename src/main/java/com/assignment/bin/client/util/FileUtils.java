package com.assignment.bin.client.util;

import java.io.IOException;
import java.io.InputStream;

public class FileUtils {
    public static String readJson(String json) {
        if (!"".equals(json) && json.startsWith("file:")) {
            String filePath = json.substring("file:".length());
            InputStream iStream = null;
            try {
                // Loading properties file from the classpath
                iStream = FileUtils.class.getClassLoader()
                        .getResourceAsStream(filePath);
                if (iStream == null) {
                    throw new IOException("File not found");
                }
                StringBuilder sb = new StringBuilder();
                for (int ch; (ch = iStream.read()) != -1; ) {
                    sb.append((char) ch);
                }
                return sb.toString();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (iStream != null) {
                        iStream.close();
                    }
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return json;
    }
}
