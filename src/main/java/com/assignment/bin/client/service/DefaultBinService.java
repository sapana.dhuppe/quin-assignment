package com.assignment.bin.client.service;

import com.assignment.bin.client.util.Utils;
import com.assignment.bin.client.model.Bin;
import com.assignment.bin.client.model.BinResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.*;

public class DefaultBinService implements BinService {

	private CloseableHttpClient httpClient;
	private Utils utils;

	public DefaultBinService() {
		utils = new Utils();
		Header header = new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		List<Header> headers = new ArrayList<>();
		headers.add(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
		headers.add(new BasicHeader("X-Master-key", utils.get(Utils.PROPERTY.API_KEY)));
		httpClient = HttpClients.custom().setDefaultHeaders(headers).build();
	}

	@Override
	public BinResponse create(Bin bin) {
		HttpPost request = new HttpPost(utils.get(Utils.PROPERTY.URL));
		try {
			request.setEntity(new StringEntity(bin.getJsonData()));
			request.addHeader("X-Bin-Private", Boolean.toString(bin.isAssessibility()));
			request.addHeader("X-Bin-Name", bin.getName());
			CloseableHttpResponse response = httpClient.execute(request);

			ObjectMapper mapper = getObjectMapper();
			BinResponse binResponse = mapper.readValue(response.getEntity().getContent(), BinResponse.class);
			return binResponse;
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		} catch (ClientProtocolException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public BinResponse update(String binId, String json) {
		HttpPut request = new HttpPut(utils.get(Utils.PROPERTY.URL) + "/" + binId);
		try {
			request.setEntity(new StringEntity(json));
			CloseableHttpResponse response = httpClient.execute(request);
			ObjectMapper mapper = getObjectMapper();
			BinResponse binResponse = mapper.readValue(response.getEntity().getContent(), BinResponse.class);
			return binResponse;
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		} catch (ClientProtocolException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}


	@Override
	public BinResponse delete(String binId) {
		HttpDelete request = new HttpDelete(utils.get(Utils.PROPERTY.URL) + "/" + binId);
		try {
			CloseableHttpResponse response = httpClient.execute(request);
			ObjectMapper mapper = getObjectMapper();
			BinResponse binResponse = mapper.readValue(response.getEntity().getContent(), BinResponse.class);
			return binResponse;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public BinResponse get(String binId) {
		return getBinResponse(binId, Collections.EMPTY_MAP);
	}

	@Override
	public BinResponse get(String binId, String jsonQuery) {
		Map<String, String> headers = new HashMap<>();
		headers.put("X-JSON-Path", jsonQuery);
		return getBinResponse(binId, headers);
	}

	private BinResponse getBinResponse(String binId, Map<String, String> headers) {
		HttpGet request = new HttpGet(utils.get(Utils.PROPERTY.URL) + "/" + binId);
		try {
			for(String key : headers.keySet()) {
				request.addHeader(key, headers.get(key));
			}
			CloseableHttpResponse response = httpClient.execute(request);
			return getBinResponse(response.getEntity().getContent());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private BinResponse getBinResponse(InputStream response) throws IOException {
		ObjectMapper mapper = getObjectMapper();
		return mapper.readValue(response, BinResponse.class);
	}

	private ObjectMapper getObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper;
	}

}
