package com.assignment.bin.client.service;

import com.assignment.bin.client.model.Bin;
import com.assignment.bin.client.model.BinResponse;

public interface BinService {
	public BinResponse create(Bin request);

	public BinResponse update(String binId, String json);


	public BinResponse delete(String binId);

	public BinResponse get(String binId);

	public BinResponse get(String binId, String jsonQuery);
}
