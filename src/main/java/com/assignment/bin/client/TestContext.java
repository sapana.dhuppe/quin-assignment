package com.assignment.bin.client;

import com.assignment.bin.client.model.BinResponse;
import com.assignment.bin.client.service.BinService;
import com.assignment.bin.client.service.DefaultBinService;

import java.util.HashMap;
import java.util.Map;

public class TestContext {
    private BinService binService;
    private Map<String, Object> data;

    public BinResponse getBinResponse() {
        return binResponse;
    }

    public void setBinResponse(BinResponse binResponse) {
        this.binResponse = binResponse;
    }

    private BinResponse binResponse;

    private String binId;

    public TestContext() {
        binService = new DefaultBinService();
        data = new HashMap<>();
    }

    public void setBinId(String id) {
        this.binId = id;
    }
    public String getBinId() {
        return binId;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public BinService getBinService() {
        return this.binService;
    }
}
