package com.assignment.bin.client.model;

import com.assignment.bin.client.json.KeepAsJsonDeserializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class BinResponse {
	@JsonProperty("record")
    @JsonDeserialize(using = KeepAsJsonDeserializer.class)
    private String record;
    private Metadata metadata;
    private String message;

    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
