package com.assignment.bin.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties
public class Metadata {
	private String id;
	private String createdAt;

	private String versionsDeleted;
	@JsonProperty("private")
	private boolean accessibility;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public boolean isAccessibility() {
		return accessibility;
	}

	public void setAccessibility(boolean accessibility) {
		this.accessibility = accessibility;
	}

	public String getVersionsDeleted() {
		return versionsDeleted;
	}

	public void setVersionsDeleted(String versionsDeleted) {
		this.versionsDeleted = versionsDeleted;
	}
}
