package com.assignment.bin.client.model;

public class Bin {
	private String jsonData; // json to store
	private boolean assessibility; // public or private
	private String name; 
	private String route;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	private String id;



	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public boolean isAssessibility() {
		return assessibility;
	}

	public void setAssessibility(boolean assessibility) {
		this.assessibility = assessibility;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}
}
