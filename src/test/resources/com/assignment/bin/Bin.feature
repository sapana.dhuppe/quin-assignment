Feature: Bin API CRUD

  Scenario: Create a new bin and verify
    Given I want to create a bin with attributes
    | name | json | isPublic |
    | sapana | {"hello" : "world"} | true |
    When I create a new bin
    Then I must verify bin is created

  Scenario: Create a new bin and read json
    Given I want to create a bin with attributes
      | name | json | isPublic |
      | sapana | file:input.json | true |
    And I create a new bin
    When I read json from the bin with query "$.moduleAccess.users[*].firstName"
    Then I verify query returned matching response to "[\"Nigel\",\"Evelyn\",\"Michelle\",\"Aaron\"]"

  Scenario: Update the json in the bin and verify
    Given I want to create a bin with attributes
      | name | json | isPublic |
      | sapana | {"hello" : "world"} | true |
    And I create a new bin
    When I update the bin with json "{\"hallo\":\"wereld\"}"
    Then I must verify bin contains "{\"hallo\":\"wereld\"}"



