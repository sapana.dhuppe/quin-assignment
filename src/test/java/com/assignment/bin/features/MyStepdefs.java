package com.assignment.bin.features;

import com.assignment.bin.client.TestContext;
import com.assignment.bin.client.model.Bin;
import com.assignment.bin.client.model.BinResponse;
import com.assignment.bin.client.util.FileUtils;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.datatable.DataTable;

import static org.junit.jupiter.api.Assertions.*;

public class MyStepdefs {

    TestContext testContext;

    public MyStepdefs(TestContext context) {
        testContext = context;
    }
    @Given("I want to create a bin with attributes")
    public void iWantToCreateABinWithAttributes(DataTable dataTable) {
        System.out.println(dataTable);
        Bin bin = new Bin();
        bin.setName(dataTable.asMaps().get(0).get("name"));
        bin.setAssessibility(Boolean.parseBoolean(dataTable.asMaps().get(0).get("isPublic")));
        String json = dataTable.asMaps().get(0).get("json");
        bin.setJsonData(FileUtils.readJson(json));
        testContext.getData().put("createBinRequest", bin);
    }

    @When("I create a new bin")
    public void iCreateANewBin() {
        BinResponse response = testContext.getBinService().create((Bin) testContext.getData().get("createBinRequest"));
        assertNotNull(response.getMetadata().getId());
        testContext.setBinId(response.getMetadata().getId());
    }

    @Then("I must verify bin is created")
    public void iMustVerifyBinIsCreated() {
        BinResponse response = testContext.getBinService().get(testContext.getBinId());
        assertEquals(testContext.getBinId(), response.getMetadata().getId());

    }

    @After
    public void tidyUp() {
        assertDoesNotThrow(() -> testContext.getBinService().delete(testContext.getBinId()));
    }

    @When("I read json from the bin with query {string}")
    public void iReadJsonFromTheBinWithQuery(String arg0) {
        BinResponse response = testContext.getBinService().get(testContext.getBinId(), arg0);
        testContext.setBinResponse(response);
    }

    @Then("I verify query returned matching response to {string}")
    public void iVerifyQueryReturnedMatchingResponseTo(String expectedJson) {
        assertEquals(testContext.getBinResponse().getRecord(), expectedJson);
    }

    @When("I update the bin with json {string}")
    public void iUpdateTheBinWithJson(String json) {
        testContext.getBinService().update(testContext.getBinId(), json);
    }

    @Then("I must verify bin contains {string}")
    public void iMustVerifyBinContains(String expectedJson) {
        BinResponse response = testContext.getBinService().get(testContext.getBinId());
        assertEquals(response.getRecord(), expectedJson);
    }
}
