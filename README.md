# quin-assignment



## Assignment
The task is to create a series of automated tests that cover the CRUD-operations of jsonbin API. You will start with the documentation page here: https://jsonbin.io/api-reference (Bins API).


## JSONBin API
A simple API to store json files over internet. Apart from basic CRUD, you can get information from stored json using Json path

## Test Scenarios
### Scenario 1 - Create, Get, Delete the bin
### Scenario 2 - Create, Get specific details with json path, Delete the bin
### Scenario 3 - Create, Update, Get to verify, delete the bin

## Project structure
    It is simple java8 maven project. 
    Cucumber is used as a BDD framework to write scenarios and generate HTML report
    Scenarios are written in src/test/resources
    Step Definations are in src/test/java
    API operations, model classes are in src/main/java

##  How to 
    Install JDK 8 and Maven 3.* 
    Run tests with command - mvn test
    Report is generated in - target\cucumber-reports\Cucumber.html
